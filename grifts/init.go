package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/kickr/kickr/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
