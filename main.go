package main

import (
	"log"

	"gitlab.com/kickr/kickr/actions"
)

func main() {
	app := actions.App()
	if err := app.Serve(); err != nil {
		log.Fatal(err)
	}
}
